/*
 * BikeStore
 * Huang Wanting 2034421
 */
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0]=new Bicycle("Apple", 20, 35);
        bicycles[1]=new Bicycle("Banana", 5, 25);
        bicycles[2]=new Bicycle("Orange", 30, 40);
        bicycles[3]=new Bicycle("Strawberry", 10, 50);
        for (int i = 0; i < bicycles.length; i++) {
            System.out.println(bicycles[i]);
        }
    }
}
